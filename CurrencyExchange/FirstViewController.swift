import UIKit

class FirstViewController: UIViewController {
    let secondViewController = SecondViewController()
    let constants = Constants()
    let eurRates = ["BYN": 3.55, "EUR": 1.0, "USD": 1.12, "RUB": 109.23]
    let usdRates = ["BYN": 3.17, "EUR": 0.89, "USD": 1.0, "RUB": 97.54]
    let rubRates = ["BYN": 0.0325, "EUR": 0.0092, "USD": 0.0103, "RUB": 1.0]
    let bynRates = ["BYN": 1.0, "EUR": 0.28, "USD": 0.32, "RUB": 30.77]

//    let currencyValueDict = ["eurUsd": 1.12, "eurRub": 109.23, "eurByn": 3.55, "eurEur": 1, "usdEur": 0.89, "usdRub": 97.54, "usdByn": 3.17, "usdUsd": 1, "rubUsd": 0.0103, "rubEur": 0.0092, "rubByn": 0.0325, "rubRub": 1, "bynEur": 0.28, "bynUsd": 0.32, "bynRub": 30.77, "bynByn": 1]
    var firstButtonIsActive = false
    var secondButtonIsActive = false
    
    private lazy var firstTextField: UITextField = {
        let firstTextField = UITextField()
        firstTextField.layer.borderWidth = 1
        firstTextField.textAlignment = .center
        firstTextField.layer.cornerRadius = 10
        firstTextField.placeholder = "0.00"
        firstTextField.text = setupCurrencyChange("")
        return firstTextField
    }()
    
    private(set) lazy var firstLabel: UILabel = {
        let firstLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 60))
        firstLabel.text = constants.currencyText
        return firstLabel
    }()
    
    private lazy var firstButton: UIButton = {
        let firstButton = UIButton()
        firstButton.setTitle(constants.currencyText, for: .normal)
        firstButton.layer.backgroundColor = UIColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 1).cgColor
        firstButton.layer.cornerRadius = 10
        firstButton.setTitleColor(.black, for: .normal)
        firstButton.addTarget(self, action: #selector(firstButtonAction), for: .touchUpInside)
        return firstButton
    }()
    
    private lazy var secondTextField: UITextField = {
        let secondTextField = UITextField()
        secondTextField.layer.borderWidth = 1
        secondTextField.textAlignment = .center
        secondTextField.layer.cornerRadius = 10
        secondTextField.placeholder = "0.00"
        secondTextField.text = setupCurrencyChange("")
        return secondTextField
    }()
    
    private(set) lazy var secondLabel: UILabel = {
        let secondLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 60))
        secondLabel.text = constants.currencyText
        return secondLabel
    }()
    
    private lazy var secondButton: UIButton = {
        let secondButton = UIButton()
        secondButton.setTitle(constants.currencyText, for: .normal)
        secondButton.layer.backgroundColor = UIColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 1).cgColor
        secondButton.layer.cornerRadius = 10
        secondButton.setTitleColor(.black, for: .normal)
        secondButton.addTarget(self, action: #selector(secondButtonAction), for: .touchUpInside)
        return secondButton
    }()
    
    private lazy var firstStackView: UIStackView = {
        let firstStackView = UIStackView(arrangedSubviews: [firstLabel, firstTextField, firstButton])
        firstStackView.distribution = .fillEqually
        firstStackView.axis = .horizontal
        firstStackView.spacing = 20
        return firstStackView
    }()
    
    private lazy var secondStackView: UIStackView = {
        let secondStackView = UIStackView(arrangedSubviews: [secondLabel, secondTextField, secondButton])
        secondStackView.distribution = .fillEqually
        secondStackView.axis = .horizontal
        secondStackView.spacing = 20
        return secondStackView
    }()
    
    private lazy var generalStackView: UIStackView = {
        let generalStackView = UIStackView(arrangedSubviews: [firstStackView, secondStackView])
        generalStackView.distribution = .fill
        generalStackView.axis = .vertical
        generalStackView.spacing = 30
        return generalStackView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(generalStackView)
        view.backgroundColor = UIColor(named: "AccentColor")
        
        firstTextField.delegate = self
        secondTextField.delegate = self
        
        setupGeneralStackView()
    }
    
    func setupGeneralStackView() {
        generalStackView.translatesAutoresizingMaskIntoConstraints = false
        
        generalStackView.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor).isActive = true
        generalStackView.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor).isActive = true
        generalStackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        generalStackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20).isActive = true
    }
    
    @objc func firstButtonAction() {
        secondViewController.firstViewController = self
        self.present(secondViewController, animated: true, completion: nil)
        firstButtonIsActive = true
    }
    
    @objc func secondButtonAction() {
        secondViewController.firstViewController = self
        self.present(secondViewController, animated: true, completion: nil)
        secondButtonIsActive = true
    }
    private var itemSelected = 0
    private var itemSelected2 = 0

    func setupCurrencyChange(_ convert: String) -> String {
        var conversion: Double = 1.0
        let amount = Double(convert) ?? 0.0
        let selectedCurrency = secondViewController.currencyArr[itemSelected]
        let to = secondViewController.currencyArr[itemSelected2]
        
        switch selectedCurrency {
        case "🇧🇾 BYN":
            conversion = amount * (bynRates[to] ?? 0.0)
        case "🇪🇺 EUR":
            conversion = amount * (eurRates[to] ?? 0.0)
        case "🇺🇸 USD":
            conversion = amount * (usdRates[to] ?? 0.0)
        case "🇷🇺 RUB":
            conversion = amount * (rubRates[to] ?? 0.0)
        default:
            print("problems")
        }
        return String(conversion)
    }
}

extension FirstViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let text = (textField.text ?? "") as NSString
        let newText = text.replacingCharacters(in: range, with: string)
        if let regex = try? NSRegularExpression(pattern: "^[0-9]*((\\.|,)[0-9]{0,2})?$",
                                                options: .caseInsensitive) {
            return regex.numberOfMatches(in: newText,
                                         options: .reportProgress,
                                         range: NSRange(location: 0,
                                                        length: (newText as NSString).length)) > 0
        }
        return false
    }
}
//
//enum CurrencyList {
//    case USD
//    case EUR
//    case RUB
//    case BYN
//}
//enum Currency {
//    case eurToUsd
//    case eurToRub
//    case eurToByn
//    case eurToEur
//    case usdToEur
//    case usdToRub
//    case usdToByn
//    case usdToUsd
//    case rubToUsd
//    case rubToEur
//    case rubToByn
//    case rubToRub
//    case bynToEur
//    case bynToUsd
//    case bynToRub
//    case bynToByn
//}

