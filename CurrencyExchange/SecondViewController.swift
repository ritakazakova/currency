import UIKit

class SecondViewController: UIViewController {
    
    let currency = Constants()
    weak var firstViewController: FirstViewController?
    let currencyArr = ["🇧🇾 BYN", "🇪🇺 EUR", "🇺🇸 USD", "🇷🇺 RUB"]
    
    private lazy var usdButton: UIButton = {
        let usdButton = UIButton()
        usdButton.setTitle("\(currencyArr[2])", for: .normal)
        usdButton.layer.backgroundColor = UIColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 1).cgColor
        usdButton.layer.cornerRadius = 10
        usdButton.setTitleColor(.black, for: .normal)
        usdButton.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        return usdButton
    }()
    
    private lazy var eurButton: UIButton = {
        let eurButton = UIButton()
        eurButton.setTitle("\(currencyArr[1])", for: .normal)
        eurButton.layer.backgroundColor = UIColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 1).cgColor
        eurButton.layer.cornerRadius = 10
        eurButton.setTitleColor(.black, for: .normal)
        eurButton.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        return eurButton
    }()
    
    private lazy var rubButton: UIButton = {
        let rubButton = UIButton()
        rubButton.setTitle("\(currencyArr[3])", for: .normal)
        rubButton.layer.backgroundColor = UIColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 1).cgColor
        rubButton.layer.cornerRadius = 10
        rubButton.setTitleColor(.black, for: .normal)
        rubButton.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        return rubButton
    }()
    
    private lazy var bynButton: UIButton = {
        let bynButton = UIButton()
        bynButton.setTitle("\(currencyArr[0])", for: .normal)
        bynButton.layer.backgroundColor = UIColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 1).cgColor
        bynButton.layer.cornerRadius = 10
        bynButton.setTitleColor(.black, for: .normal)
        bynButton.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        return bynButton
    }()
    
    private lazy var currencyStackView: UIStackView = {
        let currencyStackView = UIStackView(arrangedSubviews: [usdButton, eurButton, rubButton, bynButton])
        currencyStackView.distribution = .fill
        currencyStackView.axis = .vertical
        currencyStackView.spacing = 30
        return currencyStackView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(currencyStackView)
        view.backgroundColor = UIColor(named: "AccentColor")
        
        setupCurrencyStackView()
    }
    
    func setupCurrencyStackView() {
        currencyStackView.translatesAutoresizingMaskIntoConstraints = false
        
        currencyStackView.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor).isActive = true
        currencyStackView.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor).isActive = true
        currencyStackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -70).isActive = true
        currencyStackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 70).isActive = true
    }
    
    @objc func buttonAction(_ sender: UIButton) {
        if firstViewController?.firstButtonIsActive == true {
            if sender == usdButton {
                firstViewController?.firstLabel.text = currencyArr[2]
                self.dismiss(animated: true, completion: nil)
                firstViewController?.firstButtonIsActive = false
            } else if sender == eurButton {
                firstViewController?.firstLabel.text = currencyArr[1]
                self.dismiss(animated: true, completion: nil)
                firstViewController?.firstButtonIsActive = false
            } else if sender == rubButton {
                firstViewController?.firstLabel.text = currencyArr[3]
                self.dismiss(animated: true, completion: nil)
                firstViewController?.firstButtonIsActive = false
            } else if sender == bynButton {
                firstViewController?.firstLabel.text = currencyArr[0]
                self.dismiss(animated: true, completion: nil)
                firstViewController?.firstButtonIsActive = false
            }
        } else if firstViewController?.secondButtonIsActive == true {
            if sender == usdButton {
                firstViewController?.secondLabel.text = currencyArr[2]
                self.dismiss(animated: true, completion: nil)
                firstViewController?.secondButtonIsActive = false
            } else if sender == eurButton {
                firstViewController?.secondLabel.text = currencyArr[1]
                self.dismiss(animated: true, completion: nil)
                firstViewController?.secondButtonIsActive = false
            } else if sender == rubButton {
                firstViewController?.secondLabel.text = currencyArr[3]
                self.dismiss(animated: true, completion: nil)
                firstViewController?.secondButtonIsActive = false
            } else if sender == bynButton {
                firstViewController?.secondLabel.text = currencyArr[0]
                self.dismiss(animated: true, completion: nil)
                firstViewController?.secondButtonIsActive = false
            }
        }
    }
}
